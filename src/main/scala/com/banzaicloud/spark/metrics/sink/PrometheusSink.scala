/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.banzaicloud.spark.metrics.sink

import java.net.{InetAddress, URI}
import java.util
import java.util.Properties
import java.util.concurrent.TimeUnit

import com.banzaicloud.metrics.prometheus.client.exporter.PushGatewayWithTimestamp
import com.codahale.metrics._
import io.prometheus.client.CollectorRegistry
import io.prometheus.client.dropwizard.DropwizardExports
import org.apache.spark.internal.Logging
import org.apache.spark.metrics.sink.Sink
import org.apache.spark.{SecurityManager, SparkConf, SparkEnv}

import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
import scala.util.Try


class PrometheusSink(
                      val property: Properties,
                      val registry: MetricRegistry,
                      securityMgr: SecurityManager)
  extends Sink with Logging {

  protected class Reporter(registry: MetricRegistry)
    extends ScheduledReporter(
      registry,
      "prometheus-reporter",
      MetricFilter.ALL,
      TimeUnit.SECONDS,
      TimeUnit.MILLISECONDS) {

    val defaultSparkConf: SparkConf = new SparkConf(true)

    var metrics: Set[Map[String, String]] = Set()

    override def report(
                         gauges: util.SortedMap[String, Gauge[_]],
                         counters: util.SortedMap[String, Counter],
                         histograms: util.SortedMap[String, Histogram],
                         meters: util.SortedMap[String, Meter],
                         timers: util.SortedMap[String, Timer]): Unit = {

      // SparkEnv may become available only after metrics sink creation thus retrieving
      // SparkConf from spark env here and not during the creation/initialisation of PrometheusSink.
      val sparkConf: SparkConf = Option(SparkEnv.get).map(_.conf).getOrElse(defaultSparkConf)


      val sparkAppId: Option[String] = sparkConf.getOption("spark.app.id")
      val sparkAppName: Option[String] = sparkConf.getOption("spark.app.name")
      val executorId: Option[String] = sparkConf.getOption("spark.executor.id")

      logInfo(s"sparkAppId=$sparkAppId, " + s"sparkAppName=$sparkAppName, executorId=$executorId")

      val role: String = (sparkAppId, executorId) match {
        case (Some(_), Some("driver")) | (Some(_), Some("<driver>"))=> "driver"
        case (Some(_), Some(_)) => "executor"
        case _ => "shuffle"
      }

      // A collection of instances with the same purpose, a process replicated for scalability or reliability
      val job: String = sparkAppId.get

      // An endpoint you can scrape, usually corresponding to a single process
      val instance: String = InetAddress.getLocalHost.getHostName

      logInfo(s"role=$role, job=$job, instance=$instance")

      val executorIdMap = executorId match {
        case Some(id) => Map("id" -> id)
        case _ => Map()
      }

      val groupingKey: Map[String, String] =
        Map(
          "instance" -> instance,
          "role"     -> role,
          "appName"  -> sparkAppName.getOrElse(""),
          "appId"    -> sparkAppId.getOrElse("")
        ) ++ executorIdMap

      metrics += (groupingKey ++ Set("job" -> job))

      val metricTimestamp = if (enableTimestamp) Some(s"${System.currentTimeMillis}") else None

      pushGateway.pushAdd(pushRegistry, job, groupingKey.asJava, metricTimestamp.orNull)
    }

    override def stop(): Unit = {
      super.stop()

      logInfo(s"Deleting prometheus metrics")

      val predicate = (map:(String, String)) => map._1 == "job"

      for (metric <- metrics) {
        val jobGroupingKey = metric.find(predicate)
        val otherGroupingKeys = metric.filterNot(predicate)

        val groupingKeyUrl = metric.flatMap(gk => List(gk._1, gk._2)).mkString("/")

        jobGroupingKey match {
          case Some((_, job)) => {
            logInfo(s"Deleting prometheus metric $groupingKeyUrl")
            pushGateway.delete(job, otherGroupingKeys)
          }
          case None => logError(s"Failed to delete prometheus metric $groupingKeyUrl")
        }
      }
    }

    private def getGroupingKey(labelNames: List[String], labelValues: List[String]): Map[String, String] = {
      if (labelNames.length != labelValues.length) {
        logError(s"Label names must be the same length as label values")
        return Map()
      }

      var groupingKey = Map[String, String]()
      for (index <- labelNames.indices) {
        val name = labelNames.get(index)
        val value = labelValues.get(index)
        println(s"reading name $name with value $value")

        groupingKey = groupingKey ++ Map(name -> value)
      }

      groupingKey
    }
  }

  val DEFAULT_PUSH_PERIOD: Int = 10
  val DEFAULT_PUSH_PERIOD_UNIT: TimeUnit = TimeUnit.SECONDS
  val DEFAULT_PUSHGATEWAY_ADDRESS: String = "127.0.0.1:9091"
  val DEFAULT_PUSHGATEWAY_ADDRESS_PROTOCOL: String = "http"
  val PUSHGATEWAY_ENABLE_TIMESTAMP: Boolean = false

  val KEY_PUSH_PERIOD = "period"
  val KEY_PUSH_PERIOD_UNIT = "unit"
  val KEY_PUSHGATEWAY_ADDRESS = "pushgateway-address"
  val KEY_PUSHGATEWAY_ADDRESS_PROTOCOL = "pushgateway-address-protocol"
  val KEY_PUSHGATEWAY_ENABLE_TIMESTAMP = "pushgateway-enable-timestamp"


  val pollPeriod: Int =
    Option(property.getProperty(KEY_PUSH_PERIOD))
      .map(_.toInt)
      .getOrElse(DEFAULT_PUSH_PERIOD)

  val pollUnit: TimeUnit =
    Option(property.getProperty(KEY_PUSH_PERIOD_UNIT))
      .map { s => TimeUnit.valueOf(s.toUpperCase) }
      .getOrElse(DEFAULT_PUSH_PERIOD_UNIT)

  val pushGatewayAddress: String =
    Option(property.getProperty(KEY_PUSHGATEWAY_ADDRESS))
      .getOrElse(DEFAULT_PUSHGATEWAY_ADDRESS)

  val pushGatewayAddressProtocol: String =
    Option(property.getProperty(KEY_PUSHGATEWAY_ADDRESS_PROTOCOL))
      .getOrElse(DEFAULT_PUSHGATEWAY_ADDRESS_PROTOCOL)

  val enableTimestamp: Boolean =
    Option(property.getProperty(KEY_PUSHGATEWAY_ENABLE_TIMESTAMP))
      .map(_.toBoolean)
      .getOrElse(PUSHGATEWAY_ENABLE_TIMESTAMP)

  // validate pushgateway host:port
  Try(new URI(s"$pushGatewayAddressProtocol://$pushGatewayAddress")).get

  checkMinimalPollingPeriod(pollUnit, pollPeriod)

  logInfo("Initializing Prometheus Sink...")
  logInfo(s"Metrics polling period -> $pollPeriod $pollUnit")
  logInfo(s"Metrics timestamp enabled -> $enableTimestamp")
  logInfo(s"$KEY_PUSHGATEWAY_ADDRESS -> $pushGatewayAddress")
  logInfo(s"$KEY_PUSHGATEWAY_ADDRESS_PROTOCOL -> $pushGatewayAddressProtocol")

  val pushRegistry: CollectorRegistry = new CollectorRegistry()
  val sparkMetricExports: DropwizardExports = new DropwizardExports(registry)
  val pushGateway: PushGatewayWithTimestamp =
    new PushGatewayWithTimestamp(s"$pushGatewayAddressProtocol://$pushGatewayAddress")

  val reporter = new Reporter(registry)

  override def start(): Unit = {
    sparkMetricExports.register(pushRegistry)
    reporter.start(pollPeriod, pollUnit)
  }

  override def stop(): Unit = {
    reporter.stop()
    pushRegistry.unregister(sparkMetricExports)
  }

  override def report(): Unit = {
    reporter.report()
  }

  private def checkMinimalPollingPeriod(pollUnit: TimeUnit, pollPeriod: Int) {
    val period = TimeUnit.SECONDS.convert(pollPeriod, pollUnit)
    if (period < 1) {
      throw new IllegalArgumentException("Polling period " + pollPeriod + " " + pollUnit +
        " below than minimal polling period ")
    }
  }
}
